from rest_framework import serializers
from blog.models import Post, Comment
from .models import myUser


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = myUser
        fields = ('username', 'password')


class PostSerializer(serializers.ModelSerializer):
    comments = serializers.StringRelatedField(many=True)

    class Meta:
        model = Post
        fields = ('user', 'text', 'date', 'id', 'comments')


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('user', 'post', 'text', 'id')
