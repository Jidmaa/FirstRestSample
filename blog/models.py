from django.contrib.auth.models import User
from django.db import models


class myUser(User):
    pass

class Post(models.Model):
    user = models.ForeignKey(myUser,on_delete=models.CASCADE)
    text = models.CharField(max_length=280)
    date = models.DateTimeField(auto_now=True)
    id = models.AutoField(primary_key=True)

class Comment(models.Model):
    user = models.ForeignKey(myUser,on_delete=models.CASCADE)
    post = models.ForeignKey(Post,related_name='comments',on_delete=models.CASCADE)
    text = models.CharField(max_length=280)
    date = models.DateTimeField(auto_now=True)
    id = models.AutoField(primary_key=True)

    class Meta:
        unique_together = ('post', 'id')
        ordering = ['id']

    def __str__(self):
        return '%s: %s' % (self.user.username, self.text)