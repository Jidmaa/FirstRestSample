from django.test import TestCase
from .models import Comment, myUser, Post
from rest_framework.test import APIRequestFactory


# class Test(TestCase):
#     def test_signup(self):
#         myUser.objects.create(username="majid", password="majid")
#         myUser.objects.create(username="ali", password="ali")
#         myUser.objects.create(username="leila", password="leila")
#
#     def test_token(self):
#         factory = APIRequestFactory()
#         request = factory.post('/token/', {'username': 'majid', 'password': 'majid'})
#         request = factory.post('/token/', {'username': 'ali', 'password': 'ali'})
#         request = factory.post('/token/', {'username': 'leila', 'password': 'leila'})
#
#     def test_loginUser(self):
#         factory = APIRequestFactory()
#         request = factory.post('/login/', {'username': 'majid', 'password': 'majid'})
#         request = factory.post('/login/', {'username': 'ali', 'password': 'ali'})
#         request = factory.post('/login/', {'username': 'leila', 'password': 'leila'})
#
#     def test_post(self):
#         Post.objects.create(user=myUser.objects.get(username="majid"), text="Hi")
#         Post.objects.create(user=myUser.objects.get(username="ali"), text="Hey")
#         Post.objects.create(user=myUser.objects.get(username="leila"), text="Hello")
#
#     def test_comment(self):
#         Comment.objects.create(user=myUser.objects.get(username="majid"), post="1", text="Hi")
#         Comment.objects.create(user=myUser.objects.get(username="ali"), post="1", text="Hey")
#         Comment.objects.create(user=myUser.objects.get(username="leila"), post="1", text="Hello")

class Test(TestCase):
    def test_one(self):
        factory = APIRequestFactory()
        myUser.objects.create(username="majid", password="majid")
        request = factory.post('/token/', {'username': 'majid', 'password': 'majid'})
        request1 = factory.post('/login/', {'username': 'majid', 'password': 'majid'})
        post_id = Post.objects.create(user=myUser.objects.get(username="majid"), text="Hi")
        print(1, Post.objects.first().id)
        Comment.objects.create(user=myUser.objects.get(username="majid"), post=post_id, text="Hi")
        print(1, Comment.objects.first().id)

    def test_two(self):
        factory = APIRequestFactory()
        myUser.objects.create(username="ali", password="ali")
        request = factory.post('/token/', {'username': 'ali', 'password': 'ali'})
        request1 = factory.post('/login/', {'username': 'ali', 'password': 'ali'})
        post_id = Post.objects.create(user=myUser.objects.get(username="ali"), text="Hi")
        print(2, Post.objects.first().id)
        Comment.objects.create(user=myUser.objects.get(username="ali"), post=post_id, text="Hi")
        print(2, Comment.objects.first().id)

    def test_three(self):
        factory = APIRequestFactory()
        myUser.objects.create(username="leila", password="leila")
        request = factory.post('/token/', {'username': 'leila', 'password': 'leila'})
        request1 = factory.post('/login/', {'username': 'leila', 'password': 'leila'})
        post_id = Post.objects.create(user=myUser.objects.get(username="leila"), text="Hi")
        print(3, Post.objects.first().id)
        Comment.objects.create(user=myUser.objects.get(username="leila"), post=post_id, text="Hi")
        print(3, Comment.objects.first().id)


if __name__ == '__main__':
    Test()
