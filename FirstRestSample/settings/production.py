from .base import *

DEBUG = False
DEBUG_PROPAGATE_EXCEPTIONS = False

ALLOWED_HOSTS = ['*']
# This setting helps with debugging errors/exceptions raised while rendering the templates.
# If it sets to True Django would show error page with traceback
TEMPLATE_DEBUG = False
VIEW_TEST = False
SKIP_CSRF_MIDDLEWARE = False

# Forwards HTTP requests to HTTPS
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# If set to a non-zero integer value, the SecurityMiddleware sets the
# HTTP Strict Transport Security header on all responses that do not already have it.
SECURE_HSTS_SECONDS = 60

# If True, the SecurityMiddleware sets the X-XSS-Protection: 1; mode=block header
# on all responses that do not already have it.
SECURE_BROWSER_XSS_FILTER = True

# If True, the SecurityMiddleware sets the X-Content-Type-Options: nosniff header
# on all responses that do not already have it.
SECURE_CONTENT_TYPE_NOSNIFF = True

SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_HSTS_PRELOAD = True
SESSION_COOKIE_SECURE = True
SESSION_COOKIE_HTTPONLY = True
CSRF_COOKIE_SECURE = True
CSRF_COOKIE_HTTPONLY = True
AWS_ACCESS_KEY_ID = ""
AWS_SECRET_ACCESS_KEY = ""
AWS_STORAGE_BUCKET_NAME = ""
AWS_AUTO_CREATE_BUCKET = True
AWS_QUERYSTRING_AUTH = False
AWS_EXPIRY = 60 * 60 * 24 * 7
AWS_PRELOAD_METADATA = True

# If a URL path matches a regular expression in this list, the request will not be redirected to HTTPS
# If SECURE_SSL_REDIRECT was True
SECURE_REDIRECT_EXEMPT = []

# If True redirects all non-HTTPS requests to HTTPS
SECURE_SSL_REDIRECT = True

# If SECURE_SSL_REDIRECT was True all SSL redirects will be directed to this host
SECURE_SSL_HOST = "secure.example.com"

STATICFILES_STORAGE = ""

SERVER_EMAIL = ""

EMAIL_BACKEND = ""

ADMIN_URL = ""

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('DATABASE_NAME'),
        'USER': os.environ.get('DATABASE_USER'),
        'PASSWORD': os.environ.get('DATABASE_PASSWORD'),
        'HOST': os.environ.get('DATABASE_HOST'),
        'PORT': os.environ.get('DATABASE_PORT'),

    }
}

# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators
